# ZSH ( using Oh My ZSH ) on Linux and macOS

**NOTE for Linux users:** Check if `zsh` is already present on your system with ` which zsh`. If not, install it from your distro’s repositories.


**NOTE for macOS users:** If you’re running macOS Mojave or earlier `zsh` should already be present on your system even if macOS defaults to `bash` Starting with macOS Catalina, Apple repleaced `bash` with `zsh` as the default shell.



#### 1. Install [Oh My ZSH](https://ohmyz.sh/)

```
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

or

```
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

#### 2. ( Optional – but useful ) Install additional plugins: `zsh-synstax-highlighting` and `zsh-autosuggestions`

```
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```
and
```
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```


#### 3. ( Optional - personal preference ) Installing `Purify` theme for `Oh My ZSH`

![enter image description here](https://camo.githubusercontent.com/0bdb3708b5dd33a02e9750548c46bb947cf746255752a5ae64fd8c23c14192d6/68747470733a2f2f692e696d6775722e636f6d2f5075336666584c2e706e67)

```
curl https://raw.githubusercontent.com/kyoz/purify/master/zsh/purify.zsh-theme --output ~/.oh-my-zsh/themes/purify.zsh-theme
```

or

```
wget https://raw.githubusercontent.com/kyoz/purify/master/zsh/purify.zsh-theme -O ~/.oh-my-zsh/themes/purify.zsh-theme
```
or
```
git clone https://github.com/kyoz/purify.git
cd purify/zsh
cp purify.zsh-theme ~/.oh-my-zsh/themes/
```
**NOTE:** Purify is also available for Vim, Terminals and so on. See more on the [Purify Github Page](https://github.com/kyoz/purify)

#### 3. Configuring zsh


Edit your `~./zshrc` config file and :

1. Change the theme to purify:
```
ZSH_THEME="purify"
```
2.  Edit the plugins section:
```
plugins=(
    git
    zsh-autosuggestions
    zsh-syntax-highlighting
)
```


#### 4. Apply chances
```
source ~/.zshrc
```

#### 5. Make zsh your default shell:
```
chsh -s $(which zsh)
```

#### 6. Now download some [nice fonts for your terminal](https://www.nerdfonts.com/) and enjoy.

#### 6.1 You can find the default Oh My Zsh themes and plugins here:

- Linux `~/.oh-my-zsh/` 
- macOS `/Users/your-username/.oh-my-zsh/`
